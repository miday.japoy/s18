// Mini-Activity:
//  1. Create an "activity" folder, an "index.html" file inside of it and link the "script.js" file.
// 	2. Create a "script.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.
// 	3. Create a "trainer" object by using object literals.
// 	4. Initialize/add the "trainer" object properties.
// 	5. Initialize/add the "trainer" object method.
// 	6. Access the "trainer" object properties using dot and square bracket notation.
// 	7 .Invoke/call the "trainer" object method.
// 	8. Create a constructor function for creating a pokemon.
// 	9. Create/instantiate several pokemon using the "constructor" function.
// 	10. Have the pokemon objects interact with each other by calling on the "tackle" method.


console.log('Hello World');


let trainer = {};

trainer.trainerName = 'Japs';
trainer.Intro = function(){
	console.log(`Hi! My name is ${this.trainerName}. A pokemon trainer.`);}
	
console.log(trainer.trainerName);
console.log(trainer.Intro());

function Pokemon(name, health, attack, armor){
	this.name = name;
	this.health = health;
	this.attack = attack;
	this.armor = armor;
	this.tackle = function(target){
		console.log(`${this.name} attacked  ${target.name}`);
		target.health  += target.armor - this.attack;
		console.log(`${target.name}'s health is now ${target.health}`);
	}
};

let slowPoke = new Pokemon("slowPoke", 100, 50, 10);
console.log(slowPoke);


let raddish = new Pokemon("raddish", 90, 65, 15);
console.log(raddish);

slowPoke.tackle(raddish);

	