
// Object

let cellphone = {
	color: "black",
	weight: "115 grams",
	model: "iPhone1o",
	brad: "Apple",
	ring: function(){
		console.log('Cellphone is ringing')
	}
};	

console.log(cellphone);

// checking type of element.
console.log(typeof(cellphone));


// other way to create an object
let car = Object();

// adding properties to object
car.color = "red";
car.model = "vios 2021";
car.drive = function() {
	console.log('car is running,')
};

console.log(car);

// this. referring to the property inside an object

// Accessing property of an obeject
//  1. objectName + . + propertyNam) = objectName.propertyName
// 	2. objectName + ['propertyName'] =  objectName[propertyName]


// Re-assigning values of property
//  objectName + . + propertyName + = + "newValue"
//    = objectName.propertyNam) = "newValue"


// Deleting properties in an object
// delete + space + objectName + . + propertyName
// 		= delete objectName.(propertyName)

/*let pokemon = {
	name: "pikachu",
	element: "electric",
	level: 3,
	health: 100,
	attack: 50,
	action: function(){
		console.log('This pokemon attack targetPokemon.');
		console.log("targetPokemon's health is now reduced.");
	}
}

console.log(pokemon);*/


// Object Constructor -
// to solve problem in creating multple object 
// is a function 

/*function Pokemon(name, level, element){
	this.name = name;
	this.level = level;
	this.element = element;
	this.health = 100 * this.level;
	this.attack = this.level * 1.25;
	this.action = function(){
		console.log(this.name + 'tackled targetPokemon');
		console.log("targetPokemon's health is now reduced.");
	}
	this.die = function(){
		console.log("Pokemon died.")
	};
}

let pikachu = new Pokemon('pikachu', 3, 'electric');

console.log(pikachu);

let ratata = new Pokemon('ratata', 3, 'earth');
console.log(ratata);

*/

function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.attack = function(target){
		console.log(`${this.name} attacked  ${target.name}`);
		target.health -=10;
		console.log(`${target.name}'s health is now ${target.health}`);
	}
};

let Pikachu = new Pokemon("Pikachu");
let Geodude = new Pokemon("Geodude");

Pikachu.attack(Geodude);


